# 城市数据

#### 数据来源

数据库数据拥有若干个来源，包括：

    - CEIC数据库 ： CEIC数据库仅有288个城市，由多个数据来源组合，因此在整理过程中会可能会出现纰漏

    - 60年统计资料： 时间跨度为1949年-2008年，原始数据按照分省、分变量的形式制表

    - 地市县财政统计资料： 包括财政收入、财政支出、转移支付，最小单位为县级。时间跨度为1993年-2009年，其中2005年以前（含）有较为详细的细项数据。

    - 区域统计年鉴： 时间跨度为2001-2009，2010年-2014年有caj版本数据

#### 项目结构

------------ README.md   说明文档

  |--------- code        代码

  |--------- data        处理好的数据 

  |--------- original    原始数据


#### 记录

18-04-27 

提交了180427city_part_one.csv文件，

包括gdp、gdp三产、gdp指数；

一般预算收入、税收收入、个人所得税、企业所得税、增值税、营业税；

一般预算支出、以及若干项支出；

固话、移动电话、零售等信息